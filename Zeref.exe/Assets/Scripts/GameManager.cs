﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject player;
    public GameObject hive;
    public GameObject virusPrefab;
    public GameObject healthPoint;



    // Start is called before the first frame update
    void Start()
    {
        player.GetComponent<PlayerMovement>();
        hive.GetComponent<Hive>();
        BeginGame();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    void BeginGame()
    {
        player.GetComponent<PlayerMovement>().healthBar.fillAmount = 1f / 1f;
        SpawnVirus();
        DestroyExistingHealth();
        Instantiate(healthPoint, new Vector3(35.5f, -1.5f, 0.0f), Quaternion.identity);
        Instantiate(healthPoint, new Vector3(149.5f, 16.5f, 0.0f), Quaternion.identity);
        Instantiate(healthPoint, new Vector3(249.5f, -35.5f, 0.0f), Quaternion.identity);
        Instantiate(healthPoint, new Vector3(294.5f, -78.5f, 0.0f), Quaternion.identity);
        Instantiate(healthPoint, new Vector3(363.5f, -88.5f, 0.0f), Quaternion.identity);
    }

    void SpawnVirus()
    {
        DestroyExistingVirus();
        Instantiate(virusPrefab, new Vector3(23.9f, -1.5f, 0.0f), Quaternion.identity);
        Instantiate(virusPrefab, new Vector3(143.6f, 22.4f, 0.0f), Quaternion.identity);
        Instantiate(virusPrefab, new Vector3(228.3f, -20.5f, 0.0f), Quaternion.identity);
        Instantiate(virusPrefab, new Vector3(283.9f, -30.5f, 0.0f), Quaternion.identity);
        Instantiate(virusPrefab, new Vector3(338.5f, -78.5f, 0.0f), Quaternion.identity);
    }

    void DestroyExistingVirus()
    {
        GameObject[] viruses =
            GameObject.FindGameObjectsWithTag("Virus");

        foreach (GameObject current in viruses)
        {
            GameObject.Destroy(current);
        }
    }
    void DestroyExistingHealth()
    {
        GameObject[] healthPoints =
            GameObject.FindGameObjectsWithTag("Heal");

        foreach(GameObject current in healthPoints)
        {
            GameObject.Destroy(current);
        }
    }
}
