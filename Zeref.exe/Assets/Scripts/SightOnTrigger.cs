﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SightOnTrigger : MonoBehaviour
{
    public Enemy virusAI;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("Player"))
        {
            virusAI.inSight = true;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("Player"))
        {
            virusAI.inSight = false;
        }
    }
}
