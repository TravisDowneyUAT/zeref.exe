﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerTutorial : MonoBehaviour
{
    public GameObject testEnemy;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        testEnemy.SetActive(false);
        player.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Return))
        {
            player.SetActive(true);
            FindObjectOfType<AudioManager>().Play("WalkTutorial");
        }

    }

    void OnGUI()
    {
        GUI.Label(new Rect(300, 300, 300, 300), "Please press the Enter key");
    }

    
}
