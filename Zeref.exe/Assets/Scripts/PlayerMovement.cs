﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController2D controller;
    public Animator animator;
    public Image healthBar;
    public float startHealth = 100f;
    public float health;
    private GameMaster gm;
    
    //gameobjects referencing different triggers throughout the level
    public GameObject healthPoint;
    public GameObject enemyTrigger;
    public GameObject damageTrigger;
    public GameObject explainTrigger;
    public GameObject hiveTrigger;
    public GameObject stopTrigger;
    public GameObject noTrigger;

    float horizontalMove = 0f;
    public float runSpeed = 40f;
    bool jump = false;

    private bool showDestruct = false;

    void Start()
    {
        health = startHealth;
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
            animator.SetBool("IsJumping", true);
        }
    }

    public void OnLanding()
    {
        animator.SetBool("IsJumping", false);
    }

    void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;
    }

    public void ReduceHealth(float amount)
    {
        health -= amount;

        healthBar.fillAmount = health / startHealth;

        if(health <= 0)
        {
            Destroy(gameObject);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("t1"))
        {
            FindObjectOfType<AudioManager>().Play("EnemyFirstSight");
        }
        if (col.gameObject.tag.Equals("t2"))
        {
            FindObjectOfType<AudioManager>().Play("Damage");
        }
        if (col.gameObject.tag.Equals("t3"))
        {
            FindObjectOfType<AudioManager>().Play("Explanation");
        }
        if (col.gameObject.tag.Equals("t4"))
        {
            FindObjectOfType<AudioManager>().Play("HiveInSight");
        }
        if (col.gameObject.tag.Equals("t5"))
        {
            gameObject.GetComponent<End02>().enabled = true;
            showDestruct = true;
            FindObjectOfType<AudioManager>().Play("Stop");
        }
        if (col.gameObject.tag.Equals("t6"))
        {
            FindObjectOfType<AudioManager>().Play("No");
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("t1"))
        {
            Destroy(enemyTrigger);
        }
        if (col.gameObject.tag.Equals("t2"))
        {
            Destroy(damageTrigger);
        }
        if (col.gameObject.tag.Equals("t3"))
        {
            Destroy(explainTrigger);
        }
        if (col.gameObject.tag.Equals("t4"))
        {
            Destroy(hiveTrigger);
        }
        if (col.gameObject.tag.Equals("t5"))
        {
            Destroy(stopTrigger);
            showDestruct = false;
        }
        if (col.gameObject.tag.Equals("t6"))
        {
            Destroy(noTrigger);
        }
    }

    void OnGUI()
    {
        if(showDestruct == true)
        {
            GUI.Label(new Rect(100, 100, 100, 100), "S key now self-destructs");
        }
    }
}
