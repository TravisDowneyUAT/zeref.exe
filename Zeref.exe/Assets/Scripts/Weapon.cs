﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Transform firePoint;
    public GameObject impactEffect;
    public LineRenderer lineRenderer;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            StartCoroutine(Shoot());
            FindObjectOfType<AudioManager>().Play("Shoot");
        }
    }

    IEnumerator Shoot()
    {
       RaycastHit2D hitInfo = Physics2D.Raycast(firePoint.position, firePoint.right);

        if (hitInfo)
        {
            TestEnemy testEnemy = hitInfo.transform.GetComponent<TestEnemy>();
            Enemy virus = hitInfo.transform.GetComponent<Enemy>();
            Hive hive = hitInfo.transform.GetComponent<Hive>();
            if(testEnemy != null)
            {
                testEnemy.TakeDamage(2);
            }
            if(virus != null)
            {
                virus.TakeDamage(20);
            }
            if(hive != null)
            {
                hive.TakeDamage(20);
            }
            Instantiate(impactEffect, hitInfo.point, Quaternion.identity);

            lineRenderer.SetPosition(0, firePoint.position);
            lineRenderer.SetPosition(1, hitInfo.point);
        }
        else
        {
            lineRenderer.SetPosition(0, firePoint.position);
            lineRenderer.SetPosition(1, firePoint.position + firePoint.right * 100);
        }

        lineRenderer.enabled = true;

        yield return new WaitForSeconds(0.02f);

        lineRenderer.enabled = false;
    }


}
