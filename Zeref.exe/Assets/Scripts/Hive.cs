﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Hive : MonoBehaviour
{
    public float startHealth = 100f;
    private float health;
    public Image healthBar;
    public GameObject deathEffect;


    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        healthBar.fillAmount = health / startHealth;

        if(health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
        SceneManager.LoadScene("EndScreen01");
        FindObjectOfType<AudioManager>().Play("Congratulations");
    }
}
