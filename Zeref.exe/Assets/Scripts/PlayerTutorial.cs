﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerTutorial : MonoBehaviour
{
    public CharacterController2D controller;
    public Animator animator;
    public GameObject testEnemy;
    public GameObject walkTrigger;
    public GameObject jumpTrigger;
    public GameObject shootTrigger;

    float horizontalMove = 0f;
    public float runSpeed = 40f;
    bool jump = false;

    private bool showWalk = false;
    private bool showJump = false;
    private bool showShoot = false;

    // Update is called once per frame
    void Update()
    {
       horizontalMove =  Input.GetAxisRaw("Horizontal") * runSpeed;

        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
            animator.SetBool("IsJumping", true);
        }
    }

    public void OnLanding()
    {
        animator.SetBool("IsJumping", false);
    }

    void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("Finish"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        if (col.gameObject.tag.Equals("Walk"))
        {
            showWalk = true;
        }
        if (col.gameObject.tag.Equals("Jump"))
        {
            showJump = true;
            FindObjectOfType<AudioManager>().Play("JumpTutorial");
        }
        if (col.gameObject.tag.Equals("Shoot"))
        {
            showShoot = true;
            FindObjectOfType<AudioManager>().Play("ShootTutorial");
            testEnemy.SetActive(true);

        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("Walk"))
        {
            showWalk = false;
            Destroy(walkTrigger);
        }
        if (col.gameObject.tag.Equals("Jump"))
        {
            showJump = false;
            Destroy(jumpTrigger);
        }
        if (col.gameObject.tag.Equals("Shoot"))
        {
            showShoot = false;
            Destroy(shootTrigger);
        }
    }

    void OnGUI()
    {
        if(showWalk == true)
        {
            GUI.Label(new Rect(100, 100, 100, 100), "Press A and D to move left and right");
        }

        if(showJump == true)
        {
            GUI.Label(new Rect(100, 100, 100, 100), "Press the W key to jump");
        }
        if(showShoot == true)
        {
            GUI.Label(new Rect(100, 100, 100, 100), "Press LMB to shoot");
        }
    }
}
