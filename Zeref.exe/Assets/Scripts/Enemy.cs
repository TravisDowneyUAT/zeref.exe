﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public Transform firePoint;
    float amount = 10f;
    public float startHealth = 100f;
    private float health;
    public Image healthBar;
    public GameObject deathEffect;
    public GameObject impactEffect;
    public LineRenderer lineRenderer;

    //animator related variables
    private Animator animator;
    public bool inSight;

    void Awake()
    {
        animator = gameObject.GetComponent<Animator>();
    }

    void Start()
    {
        health = startHealth;
    }

    private void Update()
    {
        if (inSight == true)
        {
            StartCoroutine(CoolDown());
        }
        if(inSight == false)
        {
            StopCoroutine(CoolDown());
        }
    }

    private void FixedUpdate()
    {
        animator.SetBool("playerInSight", inSight);
    }

    void Flip()
    {
        transform.Rotate(0.0f, 180.0f, 0.0f);
    }

    IEnumerator Shoot()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(firePoint.position, firePoint.right);

        if (hitInfo)
        {
            PlayerMovement player = hitInfo.transform.GetComponent<PlayerMovement>();
            if(player != null)
            {
                player.ReduceHealth(amount);
            }
            Instantiate(impactEffect, hitInfo.point, Quaternion.identity);

            lineRenderer.SetPosition(0, firePoint.position);
            lineRenderer.SetPosition(1, hitInfo.point);
        }
        else
        {
            lineRenderer.SetPosition(0, firePoint.position);
            lineRenderer.SetPosition(1, firePoint.position + firePoint.right * 100);
        }
        lineRenderer.enabled = true;

        yield return new WaitForSeconds(0.02f);

        lineRenderer.enabled = false;
    }

    IEnumerator CoolDown()
    {
        StartCoroutine(Shoot());

        yield return new WaitForSecondsRealtime(2.0f);

        StopCoroutine(Shoot());
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        healthBar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            Die();
        }

    }

    void Die()
    {
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
