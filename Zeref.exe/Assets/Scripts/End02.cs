﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class End02 : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.S))
        {
            Destroy(gameObject);
            FindObjectOfType<AudioManager>().Play("Thanks");
            SceneManager.LoadScene("EndScreen02");
        }
    }
}
