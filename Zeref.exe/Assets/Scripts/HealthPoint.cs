﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPoint : MonoBehaviour
{
    private GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("Player"))
        {
            if (player.GetComponent<PlayerMovement>().health < player.GetComponent<PlayerMovement>().startHealth)
            {
                player.GetComponent<PlayerMovement>().healthBar.fillAmount = player.GetComponent<PlayerMovement>().startHealth;
                FindObjectOfType<AudioManager>().Play("Heal");
                gameObject.SetActive(false);
            }
            else
            {
                return;
            }
        }
    }
}
